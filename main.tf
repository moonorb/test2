provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = "playground-s-11-a367b8ad"
  region      = "us-central1"
  zone        = "us-central1-a"
}

resource "google_compute_instance" "my_server" {
  name         = "master"
  machine_type = "n1-standard-2"
  tags         = ["ssh"]
  boot_disk {
    initialize_params {
      image = "centos-7-v20221102"
    }
  }
  network_interface {
    network = "default"
    access_config {}
  }

}
resource "google_compute_instance" "my_server2" {
  name         = "worker"
  machine_type = "n1-standard-2"
  tags         = ["ssh"]
  boot_disk {
    initialize_params {
      image = "centos-7-v20221102"
    }
  }
  network_interface {
    network = "default"
    access_config {}
  }
}
